
Pod::Spec.new do |spec|
  spec.name = "YolbilFramework"
  spec.version = "134.0.0"
  spec.summary = "OpenGL Map Engine"
  spec.author = "iNavi Team"
  spec.platform = :ios, "9.0"
  spec.license = "Commercial"
  spec.source = { :http => 'https://repo.inavi.us/artifactory/generic-release-local/pods/yolbil/YolbilFramework/134.0.0/YolbilFramework.framework.zip'}
  spec.vendored_frameworks = "YolbilFramework.framework"
end
